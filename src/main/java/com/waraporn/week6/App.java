package com.waraporn.week6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank waraporn = new BookBank("Waraporn", 50.0);
        waraporn.print();
        waraporn.withdraw(10.0);
        waraporn.print();

        BookBank paragon = new BookBank("Paragon", 50000.0);
        paragon.withdraw(4000.0);
        paragon.print();

        waraporn.deposit(4000.0);
        waraporn.print();

        BookBank wannaporn = new BookBank("Wannaporn",100000.0);
        wannaporn.withdraw(15000.0);
        wannaporn.print();
    }
}
